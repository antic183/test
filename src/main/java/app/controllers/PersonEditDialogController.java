package app.controllers;

import app.dal.InMemoryPersonDB;
import app.model.Person;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;

public class PersonEditDialogController {

    @FXML
    private TextField txtFieldFirstName;
    @FXML
    private TextField txtFieldLastName;

    private Stage dialogStage;
    private Person person;
    private InMemoryPersonDB db;
    private boolean okClicked = false;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setPerson(Person person) {
        this.person = person;

        if (person != null) {
            txtFieldFirstName.setText(person.getFirstName());
            txtFieldLastName.setText(person.getLastName());
        } else  {
            txtFieldFirstName.setText("");
            txtFieldLastName.setText("");
        }
    }

    public void setDb(InMemoryPersonDB db) {
        this.db = db;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void btnOkClicked() {
        if (isInputValid()) {
            String firstName = txtFieldFirstName.getText();
            String lastName = txtFieldLastName.getText();

            if (person != null) {
                db.updatePerson(person.getId(), firstName, lastName);
                okClicked = true;
            } else {
                db.addPerson(firstName, lastName);
            }

            dialogStage.close();
        }
    }

    @FXML
    private void btnCancelClicked() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (txtFieldFirstName.getText() == null || txtFieldFirstName.getText().trim().length() == 0) {
            errorMessage += "No valid first name!\n";
        }
        if (txtFieldLastName.getText() == null || txtFieldLastName.getText().trim().length() == 0) {
            errorMessage += "No valid last name!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}