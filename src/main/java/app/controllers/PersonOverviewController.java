package app.controllers;

import app.MainApp;
import app.dal.InMemoryPersonDB;
import app.model.Person;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;


public class PersonOverviewController {

    @FXML
    private TableView<Person> personTable;
    @FXML
    private TableColumn<Person, String> firstNameColumn;
    @FXML
    private TableColumn<Person, String> lastNameColumn;
    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;

    private MainApp mainApp;
    private InMemoryPersonDB db = InMemoryPersonDB.getInstance();

    private Stage dialogStage;
    private PersonEditDialogController controller;

    @FXML
    private void initialize() {
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        personTable.setItems(db.getPersonData());

        showPersonDetails(null);

        personTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showPersonDetails(newValue));

    }

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        initPersonEditDialogStage();
    }

    private void showPersonDetails(Person person) {
        if (person != null) {
            firstNameLabel.setText(person.getFirstName());
            lastNameLabel.setText(person.getLastName());
        } else {
            firstNameLabel.setText("");
            lastNameLabel.setText("");
        }
    }

    @FXML
    private void btnDeletePersonClicked() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
        Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
        if (selectedIndex >= 0) {
            //personTable.getItems().remove(selectedPerson);
            db.deletePerson(selectedPerson);
        } else {
            showNotSelectedPersonError();
        }
    }

    private void showNotSelectedPersonError() {
        Alert alert = new Alert(AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("No Selection");
        alert.setHeaderText("No Person Selected");
        alert.setContentText("Please select a person in the table.");

        alert.showAndWait();
    }

    @FXML
    private void btnEditPersonClicked() {
        int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
        Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
        if (selectedIndex >= 0) {
            dialogStage.setTitle("Edit Person");
            controller.setPerson(selectedPerson);
            dialogStage.showAndWait();

            if (controller.isOkClicked()) {
                showPersonDetails(selectedPerson);
            }
        } else {
            showNotSelectedPersonError();
        }
    }

    @FXML
    private void btnAddNewPersonClicked() {
        dialogStage.setTitle("New Person");
        controller.setPerson(null);
        dialogStage.showAndWait();
    }

    private void initPersonEditDialogStage() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/view/PersonEditDialog.fxml"));
            AnchorPane page = loader.load();

            dialogStage = new Stage();
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(mainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setDb(db);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
