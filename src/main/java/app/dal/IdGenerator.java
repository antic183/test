package app.dal;

/**
 * Created by anticmarjan on 26.02.17.
 */
class IdGenerator {
    private static long id = 0;

    protected static final long getNextId() {
        return id++;
    }
}
