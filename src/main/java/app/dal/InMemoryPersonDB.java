package app.dal;

import app.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by anticmarjan on 26.02.17.
 */
public class InMemoryPersonDB {
    private static InMemoryPersonDB inMemoryDB;
    private ObservableList<Person> personData = FXCollections.observableArrayList();

    private InMemoryPersonDB() {
        personData.add(new Person(IdGenerator.getNextId(), "Anna", "Best"));
        personData.add(new Person(IdGenerator.getNextId(), "Stefan", "Meier"));
        personData.add(new Person(IdGenerator.getNextId(), "Martin", "Mueller"));
    }

    public static final InMemoryPersonDB getInstance() {
        if (inMemoryDB == null) {
            inMemoryDB = new InMemoryPersonDB();
        }

        return inMemoryDB;
    }

    public ObservableList<Person> getPersonData() {
        System.out.println(personData.size());
        return personData;
    }

    public final void deletePerson(Person person) {
        personData.remove(person);
    }

    public final void updatePerson(double id, String firstName, String lastName) {
        personData.stream()
                .filter(p -> p.getId() == id)
                .limit(1)
                .forEach(el -> {
                    el.setFirstName(firstName);
                    el.setLastName(lastName);
                });
    }

    public final void addPerson(String firstName, String lastName) {
        personData.add(new Person(IdGenerator.getNextId(), firstName, lastName));
    }
}
