package app.dal.sqlite;

import app.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;


public class SqlitePersonDb {

    /*private static SqlitePersonDb instance;

    private final String URL;
    private final String TABLE_NAME;
    Connection conn = null;

    private ObservableList<Person> persons = FXCollections.observableArrayList();

    private SqlitePersonDb() {
        this.URL = "jdbc:sqlite:personen_verwaltung.db";
        this.TABLE_NAME = "person";

        try {
            connect();
            Statement stmt = conn.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "\n"
                    + "( id INTEGER PRIMARY KEY AUTOINCREMENT,\n"
                    + "  firstname VARCHAR NOT NULL,\n"
                    + "  lastname VARCHAR NOT NULL\n"
                    + ");";
            stmt.execute(sql);
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            disconnect();
        }
    }

    public static SqlitePersonDb getInstance() {
        if (instance == null) {
            instance = new SqlitePersonDb();
        }

        return instance;
    }

    private void connect() {
        try {
            conn = DriverManager.getConnection(URL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public ObservableList<Person> getAllPersons() {
        try {
            connect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from " + TABLE_NAME);
            persons.clear();

            while (rs.next()) {
                long id = rs.getLong("id");
                String firstName = rs.getString("firstname");
                String lastName = rs.getString("lastname");
                persons.add(new Person(id, firstName, lastName));
            }
            rs.close();
            stmt.close();
            return persons;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            disconnect();
        }
    }

    public void addPerson(String firstName, String lastName) throws Exception {
        try {
            connect();
            String query = "INSERT INTO " + TABLE_NAME + "(firstname, lastname) values(?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.execute();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            preparedStatement.close();
            if (rs.next()) {
                int last_inserted_id = rs.getInt(1);
                persons.add(new Person(last_inserted_id, firstName, lastName));
            }
        } catch (SQLException e) {
            throw new Exception("SQL Insert Exception!");
        } finally {
            disconnect();
        }
    }

    public void updatePerson(long id, String firstName, String lastName) throws Exception {
        try {
            connect();
            String query = "UPDATE " + TABLE_NAME + " SET firstname=?, lastname=? where id=(?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setLong(3, id);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new Exception("SQL Update Exception!");
        } finally {
            disconnect();
        }
    }

    public void deletePerson(long id) throws Exception {
        try {
            connect();
            String query = "DELETE FROM " + TABLE_NAME + " where id=(?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setDouble(1, id);
            preparedStatement.execute();

            Person personToRemove = null;
            for (Person p : persons) {
                if (p.getId() == id) {
                    personToRemove = p;
                }
            }
            persons.remove(personToRemove);
        } catch (SQLException e) {
            throw new Exception("SQL Delete Exception!");
        } finally {
            disconnect();
        }
    }

    private void disconnect() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

}
